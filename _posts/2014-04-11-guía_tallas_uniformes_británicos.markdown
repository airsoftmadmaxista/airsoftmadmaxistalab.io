---
layout: post
title:  "Guía de tallas de uniformes británicos actuales"
author: Cowboy DespertaFerro
date:   2014-04-11
category: Equipamiento
keywords: airsoft, howto, milsim, montañismo, echarse al monte, frugalidad, dpm, equipación, mtp, multicam británico, surplus, tallajes, tallas, uniformes, uniformidad
---

A veces resulta complicado distinguir con certeza el tallaje de los uniformes surplus (originales) extranjeros. Conocer la talla adecuada es vital, sobre todo cuando se compran por internet.



Esta tabla permite entender un poco cómo funcionan las tallas en los uniformes británicos para así poder elegir mejor la talla.



Es posible que haya algún error ya que hemos tenido que brujulear mucho y comparar con otras tallas, pero creemos que es bastante acertado. De todas formas si crees que algo está mal mándanos un correo electrónico y lo modificaremos para hacer nuestra tabla mucho más completa y exacta.



###### **Guerreras:** ######

La medida de las guerreras va dada por un número de 3 dígitos a la izquierda y otro a la derecha, separados por una barra:
~~~
170/ 88 (por ejemplo)
~~~

El primer valor indica la altura de la persona para la que está pensada ( +-5cm) y la segunda el contorno de pecho

El comprador deberá medirse el pecho por debajo de las axilas para saber cuál es su medida, y tener en cuenta que debería sobrar espacio para poder llevar ropa de abrigo debajo.



###### **Pantalones:** ######

En los pantalones tenemos 3 valores:
~~~
80/84/96 (por ejemplo)
~~~

Donde el primer valor (80) indica la medida de largo de la pierna desde la entrepierna, el segundo valor (84) nos indica la medida de cintura (en este caso equivaldría a una  42) y 96 la medida de largo total del pantalón.



<a href="/assets/img/2014-04-11-guía_tallas_uniformes_británicos/tallas_dpm_colorsintransparencia.png" target="_blank"><img src="/assets/img/2014-04-11-guía_tallas_uniformes_británicos/tallas_dpm.png" alt="Tabla de tallas de uniformes británicos" width="250"/></a>
(*) click para ampliar la tabla




---
Fuentes:
	<a href="http://www.armysurplus.es/es/tallajes-surplus" target="_blank">Army Surplus</a>
	<a href="http://www.mercabombi.es/es/content/6-tallas" target="_blank">MercaBombi</a>
