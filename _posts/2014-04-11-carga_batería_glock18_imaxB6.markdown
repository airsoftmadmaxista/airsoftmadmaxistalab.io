---
layout: post
title:  "[HowTo] Cargar batería de glock-18 eléctrica con Imax B6"
author: Cowboy DespertaFerro
date:   2014-04-11
category: Tecnología
keywords: airsoft, howto, baterías, Cyma, electrónica, glock, mecánica, pistola, primaria, réplica
---

En los transformadores el cable negativo es el negro liso (color liso solo con las inscripciones)

El positivo es el cable de color negro con simbolos (generalmente trae una raya blanca pintada). El transformador que trae de serie la glock18 no es una excepción.

Por otro lado, como inciso, en la nomenclatura eléctrica el estándard es el siguiente:
Como siempre hay dos cables.
El negro es el ... negro, es decir el negativo.
El otro será el positivo, que normalmente sería en rojo.

Por consiguiente:


Debemos separar el zócalo de carga de batería que trae el transformador de serie. Esto se hace de una forma sencilla: cortándolo con unas tijeras.

 

**Cable con raya blanca [positivo] del zócalo de carga de la batería de la glock18, a cable rojo [positivo] del cargador Imax B6**


**Cable negro total [negativo] del zócalo de carga de la batería de la glock 18, a cable negro [negativo] del cargador Imax B6**

La batería que trae de serie la Glock18 de Cyma -supongo que la tokio Marui también- tiene las siguientes características:

Tipo: NiMh
500 mAh.
7,2 v.


Una vez conectados los cables, en el Imax B6:


###### **A) CARGA LENTA** ######

Configuración del cargador requerida:

~~~
USER SET

       PROGRAM->
~~~

~~~
Safety Timer

ON        420min
~~~

~~~
USB/Temp Select

Temp Cut-Off 45C
~~~

Configuración opcional sugerida:

~~~
USER SET

       PROGRAM->
~~~

~~~
Capacity Cut-Off

ON         640mA
~~~

Ajustes de la carga:

~~~
PROGRAM SELECT

       NiMH BATT
~~~

~~~
NiMH CHARGE  Man

CURRENT     0.1A
~~~




###### **B) CARGA RÁPIDA** ######
~~~
USER SET

       PROGRAM->
~~~

~~~
NiMH Sensitivity

D.Peak   Default
~~~

~~~
Safety Timer

OFF       ...min
~~~

~~~
USB/Temp Select

Temp Cut-Off 45C
~~~

Configuración opcional sugerida:
~~~
USER SET

       PROGRAM->
~~~

~~~
Capacity Cut-Off

ON         620mA
~~~

Ajustes de la carga:
~~~
PROGRAM SELECT

       NiMH BATT
~~~

~~~
NiMH CHARGE  Man

CURRENT     0.2A
~~~
