---
layout: post
title:  "Una mirada crítica: Esclavos del *gear*, del equipamiento"
author: Cowboy DespertaFerro
date:   2015-12-08
category: Equipamiento
keywords: equipamiento, equipación, uniformes, uniformidad, A.L.I.C.E., MOLLE, correajes, airsoft, equipación, formación, milsim, radio, réplicas, dummies, ignorancia, consumismo creditófago, cosas
---

##### **"No lleves nada que no seas capaz de gestionar por tí mismo"** #####

Esa máxima, que ya desde pequeñitos deberíamos haber conocido e interiorizado, dota de autosuficiencia al individuo y lo hace más útil para sus semejantes.


Su desconocimiento o su no aplicación redunda en:

* El individuo es una carga / distracción para el resto de la unidad, que constantemente debe ayudarle a gestionar su equipación: extrayendo/colocando cosas, asesorándole en su empleo (de qué sirve una costosa radio si no la sabes utilizar con fluidez), etcétera.
* El individuo no rinde, es ineficiente: va incómodo (cuando no sobrecargado), no tiene las cosas cuando las necesita, deja de utilizarlas por no saber extraerlas o utilizarlas, siente vergüenza incluso.
* El individuo rompe o pierde cosas, lo cual redunda en más ineficiencia y un duro golpe a su bolsillo. Esto es desmoralizante.
* El individuo se hace daño y/o hace que otros se accidenten.


**Esto y el "Compañerismo"**:

Muchos son capaces de regalar las últimas gotas de agua de su cantimplora, quitarse el pan de la boca para dárselo a un compañero que lo necesite, sacrificar su vivac, sus minutos de sueño... pero se mosquean cuando detectan las cosas que señalo más arriba. Y es con razón.




##### **Gestionar tus propias cosas** #####


Se dice que *una persona feliz no consume*, no experimenta el imperativo de comprar cosas porque sí.

Esto no implica que deba acudir desnudo a las partidas, no. Pero analizando este afición nuestra -el airsoft- veremos que realmente con munición, réplica, uniforme y botas es suficiente para causar bajas.


Partiendo de ahí, considero que no debo comprar nada que:

* No sepa usar
* No vaya a usar
* No sepa transportar (durante muchas horas: correr con ello, reptar con ello, trepar con ello)
* No pueda transportar (idem)
* No sea realmente útil y duradero

(\*) Duradero: no por ser de marca y/o caro es bueno; todo el mundo sabe que existe material de combate procedente de *surplus* que le da mil patadas en rendimiento y en precio a material "de marca". Harto estoy de ver multicam rosas, y culos de pantalón pelados al deteriorase el elastán. Toda cosa tiene una función.


Y yo creo que de esta forma se evita ir sobrecargado, accidentarse, desmoralizarse, deteriorar material, restar al conjunto de la unidad.


Considero que hay que pensar muy bien antes de comprar, antes de añadir cosas a nuestro equipo. Una vez adquiridas, no está de más **tomarse todo el tiempo necesario en probarlas, ubicarlas... fijarse y cuestionar dónde y cómo las colocan las unidades reales**. No se debería tener miedo de admitir que una adquisición ha sido un error, y descartarla, deshacerse de ella.


Probar previa y exhaustivamente una herramienta hace que uno cuando salta al terreno ya sepa cómo se usa, sea eficiente, haya adquirido la habilidad. Esto es de especial relevancia en el caso del armamento: la puntería y la rapidez es importante si quieres hacer bien el trabajo y dificultar el del que tienes enfrente, que desea fervientemente despacharte.


Recuerdo que en una entrevista que se le hizo a un mando del ejército de Vietnam del Norte, éste señaló una de las grandes debilidades del moderno ejército estadounidense: eran soldados profesionales, bien entrenados, pero iban sobrecargados y esto les lastraba en un terreno para el que evidentemente no estaban totalmente preparados.


Por eso, para finalizar, tan importante es la gestión de la indumentaria como **saber dónde uno se mete**... pero eso ya es otra historia (volúmen 2).
