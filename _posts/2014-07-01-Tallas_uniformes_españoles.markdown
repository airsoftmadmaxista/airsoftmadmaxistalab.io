---
layout: post
title:  "Tallas de los uniformes españoles"
author: Cowboy DespertaFerro
date:   2014-07-01
category: Equipamiento
keywords: airsoft, howto, milsim, mochila, montañismo, echarse al monte, frugalidad, camuflaje, equipación, España, tallajes, tallas, uniformidad, uniformes
---

El número equivale al **perímetro del pecho**:

1=de 92 a 97 cm (S)

2=de 98 a 103 cm (M)

3=de 104 a 109 cm (L)

4=de 110 a 115 cm (XL)

5=de 116 a 121 cm (XXL)

 

La Letra equivale a la **altura**:

C=de 1'65 a 1'69 m (Corto)

N=de 1'70 a 1'78 m (Normal)

L=de 1'79 a 1'87 m (Largo)

M=Mujer (Equivale mas o menos a la N, aunque las camisolas son mas anchas para que quepan los pechos)



(*) Estas tallas son para uniformes de combate, los uniformes de vestir, chándals, etcétera ser rigen por otro tipo de numeración
