---
layout: post
title:  "Comunicaciones: (PMR) tabla de frecuencias y subtonos ctss"
author: Cowboy DespertaFerro
date:   2014-05-26
category: Técnica
keywords: pmr, radio, frecuencias, subtonos, ctss, comunicaciones, Puxing, Baofeng, Pofung, Midland, airsoft, canales, walkie
---


<a href="/assets/img/2014-06-24-tabla_frecuencias_subtonos_pmr/frecuencias canales.jpeg" target="_blank"><img src="/assets/img/2014-06-24-tabla_frecuencias_subtonos_pmr/frecuencias canales.png" alt="Tabla de FRECUENCIAS - CANALES" width="250"/></a>

Tabla de FRECUENCIAS - CANALES
(*) click para ampliar


<a href="/assets/img/2014-06-24-tabla_frecuencias_subtonos_pmr/frecuencias subtonos_ctss.jpeg" target="_blank"><img src="/assets/img/2014-06-24-tabla_frecuencias_subtonos_pmr/frecuencias subtonos_ctss.png" alt="Tabla de FRECUENCIAS - SUBTONOS" width="250"/></a>

Tabla de FRECUENCIAS - SUBTONOS
(*) click para ampliar
