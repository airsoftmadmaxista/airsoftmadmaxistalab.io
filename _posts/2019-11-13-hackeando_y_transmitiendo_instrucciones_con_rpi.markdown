---
layout: post
title:  "jaqueando y radiando instrucciones con RPI. (bonus radio DMR)"
author: Cowboy DespertaFerro
date:   2019-11-13
category: Técnica
keywords: simulación militar, radio, pmr, dmr, raspberry pi, hack, tecnología, contenidos, libre albedrío, hack, radiopaquetería
---
La simulación militar, salvo aquella que pretende exclusivamente ser réplica de situaciones de combate cercano (cqb y variantes), no se realiza en ratoneras. Con ratonera me refiero a terrenitos vallados, terrenitos pequeños, finquitas privadas de poca monta, llanas.

La simulación militar de la buena se lleva a cabo en terreno extenso, municipios enteros, en terreno accidentado -cuanto más mejor-.
Eso conlleva ciertos desafíos. Uno de ellos son las COMUNICACIONES.
El PMR ha demostrado ser muy problemático en esas situaciones, aunque en su defensa cabe decir que añade una dosis de sacrificio por parte de los participantes -organizadores y jugadores-, que habrán muchas veces de desplazarse a cotas más altas para comunicar, por ejemplo.


#### INTRODUCCIÓN

Como muchos de ustedes sabrán, junto al PMR existe también la radio digital móvil -DMR-.
Una simple búsqueda en la zafia Wikipedia aclarará el concepto:
> «La radio móvil digital (en inglés, digital mobile radio, DMR) es un estándar del European Telecommunications Standards Institute (ETSI), ETSI TS 102 361, publicado en el año 2005, desarrollado como protocolo de radio digital de banda estrecha, con el fin de conseguir una mejora de la eficiencia espectral sobre la radio analógica tradicional PMR y facilitando las comunicaciones bidireccionales a través de radio digital. Está basado en un protocolo que utiliza dos intervalos de tiempo desarrollado por Motorola solutions TDMA de 12,5 kHz, TDMA es usado ampliamente en GSM y TETRA.

> Hay muchas razones para elegir DMR -emisor, receptor, o ambos- como sustituto de PMR, entre ellas:

> * Eficiencia espectral.
> * Prolongación de la vida útil de la batería, al utilizar en cada llamada un intervalo TDMA, reduciendo a la mitad el consumo.
> * Utiliza técnicas de corrección de errores que regeneran la voz.
> * No transmite el ruido de fondo, evitando las molestias.
> * Mayor cobertura, al mejorar la calidad de audio.
> * Mayor confidencialidad.
> * Mejoras en la señalización.
> * Mensajes de texto.
> * Servicios de localización.
> * Telemetría.»

A la ecuación, como con el PMR, se le pueden añadir repetidores, valores añadidos (envío de datos, etc...). Hace tiempo tratamos en este blog corrosivo sobre **RADIOPAQUETERÍA** (***radiopacking***); DMR facilita mucho estas cosas.


#### PLANTEAMIENTO Y RESOLUCIÓN

El mérito no es mío, sino de varios amigos que, movidos por la incansable curiosidad y la filosofía del hazlotúmismo y el conocimiento libre, han llevado a cabo el proyecto. Yo estoy en fase de pruebas y mejora, con un emisor/receptor prestado. Aunque con una simple radio FM puedo también sintonizar la frecuencia :D
Para aumentar alcance habría que mejorar la antena y la potencia. El alcance es corto... aún.

<a href="/assets/img/2019-11-13-hackeando_y_transmitiendo_instrucciones_con_rpi/pi-fm_002.png" target="_blank"><img src="/assets/img/2019-11-13-hackeando_y_transmitiendo_instrucciones_con_rpi/pi-fm_002.png" alt="materiales_radio_rpi" width="250"/></a>

Si a un ordenador del tamaño de una cajetilla de tabaco de bajísimo consumo energético, como puede ser una Raspberry Pi, conveniente y frugalmente modificado, junto a una batería de esas que se utilizan con los móviles y una diminuta placa solar, tendremos un escenario muy interesante en lo que a comunicaciones -y misiones- se refiere.
El conjunto actuaría así como un transmisor de radio digital.

Uno de los mejores rasgos de este proyecto es que no es necesario ningún hardware adicional (a parte de la Raspberry Pi). Prácticamente basta con conectar un trozo de 20 centímetros de cable desde el pin nº 4 del GPIO para que actúe como antena y ya estará todo hecho. Un emisor/receptor será capaz de captar lo que ésta emite en la frecuencia adecuada.

<a href="/assets/img/2019-11-13-hackeando_y_transmitiendo_instrucciones_con_rpi/pi-fm_003.png" target="_blank"><img src="/assets/img/2019-11-13-hackeando_y_transmitiendo_instrucciones_con_rpi/pi-fm_003.png" alt="_radio_rpi" width="250"/></a>

El interface es sencillo y potente. Y muy barato. Una raspberry pi zero vale menos de 10$. Y si la intención es tirar la casa por la ventana :D .... una raspberry pi 3B se puede encontrar por 20$.

Por ejemplo:

Si se desea emitir un mensaje pregrabado (secuencia morse, por ejemplo :D )
~~~
>	sudo python
>	>>> import PiFm
>	>>> PiFm.play_sound("instrucciones_charlie_b.wav")
~~~

Si se desea transmitir un mensaje pregrabado en otra frecuencia:
~~~
>	sudo ./pifm instrucciones_alpha_a 100.0
~~~
(*) El sistema trabajará desde aprox. 1Mhz hasta los 250Mhz. Muchos receptores de radio requieren una señal que sea un múltiplo de 0.1 MHZ para trabajar correctamente.


Se usa el hardware de la raspberry pi (el ordenador del tercer mundo) para generar señales ("spread-spectrum clock") en los pins del GPIO para emitir a través de emisor FM.
La librería python llama a un programa escrito en C. Éste mapea el Bus periférico (0x20000000) en la memoria física al espacio de las direcciones virtuales utilizando /dev/mem y nmap. Para hacer esto requiere acceso root (de ahí el empleo de "sudo"). A continuación establece el módulo clock generator en "activado" y lo prepara para emitir en el GPIO nº 4 (ningún otro pin puede ser utilizado). Asimismo configura la frecuencia a 100.0MHz (PLLD@500Mhz dividido entre 5), lo cual constituirá la "transportadora". En este punto las radios dejan de emitir ruido blanco y permanecen silenciosas, limpias.
La "modulación" se lleva a cabo ajustando la frecuencia utilizando el divisor entre 100.025MHz y 99.975Mhz, y resulta en la señal de audio. El divisor fraccional no tiene suficiente resolución para producir más que un sonido de 6 bit. No obstante, gracias a la rapidez de la Raspberry Pi, podemos hacer oversampling para obtener un audio de 9.5 bit usando 128 subsamples por cada sample de audio real. Actualmente, en el estado del proyecto, se puede conseguir sonido de 16 bit.

<a href="/assets/img/2019-11-13-hackeando_y_transmitiendo_instrucciones_con_rpi/pi_fm.png" target="_blank"><img src="/assets/img/2019-11-13-hackeando_y_transmitiendo_instrucciones_con_rpi/pi_fm.png" alt="_radio_rpi" width="250"/></a>


#### UN EJEMPLO PRÁCTICO ####
* Una raspberry pi es totalmente controlable por ssh.
* Tiene conectividad wireless, bluetooh y LAN cableada.
* Su sistema operativo es Linux y se puede configurar a discreción.
* Cualquiera que logre conectarse por ssh a una raspberry la controla totalmente. Puede subir archivos, modificar configuraciones, etc...


El ***escenario*** es el siguiente:

Máquina Rpi zero en medio del monte.

Está configurada para actuar como punto de acceso wireless.

Asimismo la raspberry emite por radio un bucle de dos instrucciones en morse: una para el bando A y otra para el bando B. Ambos bandos conocen usuario y contraseña de acceso ssh.

Ambos contendientes conocen usuario y contraseña de acceso ssh (aunque no es necesario porque se puede hackear por fuerza bruta).

Ambos contendientes disponen de dispositivo con terminal linux (cualquier móvil es capaz de disponer de ello).

**Si una unidad logra acercarse a la cobertura inalámbrica de la máquina -rpi- podrá conectarse a ella, acceder a ella por ssh, consultar un fragmento de la codificación de coordenadas del bando rival y cambiar convenientemente el código morse que se radia al bando contrario de forma que envíe a una de sus unidades a una emboscada que no necesariamente habrá de ser en la coordenada destino :D**


Eso es sólo un ejemplo de lo que se puede llegar a hacer con un poco de imaginación y la filosofía adecuada... y también -por supuesto- de que lo mejor de la vida es el libre albedrío, la no guionización, el someterse al principio aristocrático que rige la naturaleza.