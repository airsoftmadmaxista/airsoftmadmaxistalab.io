---
layout: post
title:  "Rutina de mantenimiento de las réplicas de airsoft"
author: Cowboy DespertaFerro
date:   2014-05-26
category: Tecnología
keywords: airsoft, arandela, mecánica, réplica, shim, shiming, tecnología, mantenimiento, reparación
---

«Los componentes de una réplica, sobre todo las piezas móviles, van deteriorándose con el uso y/o el tiempo. Si no se les hace un mantenimiento, acaban hechas un desastre por dentro e, incluso, se rompen.
Muchas grasas pierden sus propiedades con el tiempo, y llegan casi a solidificarse. Aparece el óxido también, las piezas de metal y de plástico se degradan / desgastan. 
Esto en réplicas como el M14 EBR de Kart es fatal: el fabricante desapareció, no hay apenas recambios... y, por poner un ejemplo, si se rompe el tappet plate, que como es habitual es de plástico, tocará muy probablemente cambiar el gearbox entero por uno de Cyma por ejemplo.

Cuando compramos una réplica lo primero y más básico es limpiar por dentro, reengrasar, pulir y sincronizar (shiming). El coste es cuasi cero: los menos de 5€ que vale un juego de arandelas.
Yo soy partidario de comprar réplicas que, sean baratas o caras (las prefiero chinorras y baratas), la comunidad haya constatado su buen funcionamiento. Partiendo de una base decente, considero que sólo se deben sustituir piezas cuando éstas estén a punto de romperse o se hayan roto ya. Y para hacer esto es conveniente un entretenimiento de la réplica: abrirla con frecuencia para realizarle mantenimiento.

He aquí una metodología que he leído y que creo que a muchos nos servirá.


##### RIFLES DE CERROJO (de MUELLE) #####

Cada cinco o seis partidas hay que revisar:

- holguras en tornilleria y componentes exteriores (cuerpo, cañón exterior, culata...).
- el interior del conjunto de gatillo y reengrasado. Es importante detectar las posibles holguras en el gatillo, revisar en algunos modelos los rodamientos del balancín o en los L96 comprobar el estado de los muelles.
- el conjunto de precisión, limpiar si procede. Revisar el hop up buscando posibles arañazos del nozzle en la entrada a la cámara, cambiar la goma o el seam si procede. El cañón basta con pasarle baqueta con un poco de grasa (yo uso aceite de máquina de coser en una cantidad ínfima del que, tras darle caña con la baqueta y el trapillo, no quedará apenas nada impregnando el cañón.
- conjunto de compresión, limpiar y engrasar si procede. Hay que realizar la prueba de siempre para cerciorarnos de que el pistón comprime correctamente. También debemos echar un ojo a las paredes interiores del cilindro para detectar posibles arañazos o marcas.

Para el engrase yo utilizo grasa de teflón para bicicletas. La compro en el Decathlon.
Hay quién recomienda Molikote, que es muy cara pero que dicen que da gran rendimiento. Yo no la he probado.



##### AEG #####

En los DMR, normalmente cada 5 ó 6 partidas, revisión del conjunto de precisión y cada 8, revision interna

En las réplicas de asalto, normalmente cada 5 ó 6 partidas, revisión interna completa:

- Compresión (la prueba habitual de compresion del piston dentro del cilindro).
- Guías tanto por donde pasa el pistón como las de la del tappet plate.
- Dientes de los principales engranajes (vigilar su desgaste)
- Oring de la cabeza de cilindro y al interior de la misma.
- Limpieza del interior del nozzle y luego del conjunto al completo.
- Volvemos a colocar las arandelas (shims) que teníamos instaladas, si ya la teniamos sincronizada (shimeada) -si no, a qué esperas para sincronizarla-, comprobamos que todo gire perfectamente cerrando las tapitas.
- Si todo esta correcto, procedemos con el engrasado de rigor (ojo, que se dice que es igual de malo el exceso que la ausencia de lubricación).
- Cableado (cables pelados, pinzados, retorcidos, quemados...).
- Contactos del gatillo
- Búsqueda de holguras del cuerpo y corregir los pequeños desperfectos.

(*) aquí una de las cosillas que yo siempre suelo hacer y recomiendo es que si vais a sincronizar o shimear por primera vez vuestra réplica,tengais apuntado el numero y grosor de shiming que coloqueis,asi siempre teneis una referencia a la hora de volver a rearmar vuestro box,siempre puede variar por las holguras que pueden aparecer en los gearbox pero es bueno tener siempre una referencia.


##### PISTOLAS #####

A las eléctricas lo mismo exactamente que a los fusiles, quizás en éstas no tan exagerado. Revisión interna cada 2 meses.

A las de gas mantenimiento básico cada semana.

- limpieza del conjunto completo,externo y interno.
- reengrase de los mecanismos basicos internos.
- comprobacion de fugas en cargadores y reparacion si los necesitan.
- limpieza del cañón interno.


Luego también es recomendable -y muy friki- hacerse una pequeña ficha en un procesador de texto o como uno considere conveniente para ir anotando todos los cambios que se van haciendo, así como los mantenimientos de las réplicas. Así se tiene un relativo control sobre lo que necesita».

---

Fuente: zapador-1986 en 6mmsnipercomunity.es
