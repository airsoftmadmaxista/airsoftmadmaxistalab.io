---
layout: post
title:  "CAPÍTULO II. De idiotas y sus particulares mitos"
author: Cowboy DespertaFerro
date:   2019-10-13
category: Sátira
keywords: simulación militar, vicios ajenos, pataletas, bar, darwinismo, eugenesia, chupipandis, chupipandis con ínfulas
---
BATALLITAS DE MENTIRA Y CHICHA Y NABO.
(VENTURAS Y DESVENTURAS DE UN JUGADOR DE SIMULACIÓN MILITAR)

#### CAPÍTULO II. De idiotas y sus particulares mitos defraudados

- - -
Érase una vez en Felinolandia...

No ha demasiado tiempo, metidos todos en faena, en uno de esos escenarios en que las cosas andan ya del todo torcidas y el derrotismo cala en las mentes más flojas, uno -defraudado quizás por no ser secundado en su PATALETA- le reprochó a otro de esta casa:

— Se me acaba de "caer" un mito

*(Mal dicho, fruto sin duda de un momento de debilidad, porque así es como se cae aún más en la ignominia y la indigencia moral e intelectual)*.

No pude menos que encogerme de hombros y callar. Hay que defraudar más.

Porque a veces es mejor silenciar en ese preciso instante lo que uno piensa so pena de parecer más rudo de lo que uno es y colaborar en que una unidad, configurada así por capricho del destino, acabe de desmoronarse y echar por tierra el fin común, que no es otro que prevalecer.

Y yo no entiendo la manía del personal por dar cosas por hechas, preconcebir, ¿mitificar?... y siempre tratar de llevar al otro a su cortijo, a su historieta o capricho particular y porqueyolovalguista... al bar.

Menos mito y más zapato.

En simulación militar, tal como la entendemos en esta casa, cuando estás voluntariamente metido en ella hasta el pescuezo, sólo se te exigen tres cosas:
1. Mantener el pico cerrado, no lloriquear, no intercambiar excesiva saliva
2. Ser autosuficiente
3. Trabajar

Luego Dios dirá.

Si vd se involucra en lo personal y veo lorealismo le defraudaré. Siempre es un gusto hacer eso.
