---
layout: post
title: "Tratado de Miljim y Contabilidad Lechera"
author: Cowboy DespertaFerro
date: 2020-07-11
category: Sátira
keywords: miljim, contabilidad lechera, despilfarro, plutocracia, avaricia, corchopán, simulación militar
---
Muchos recordarán lo que no ha mucho costaba -en €- la Simulación Militar.  
Cuenta una antigua leyenda que por menos de 40€ había operaciones bien planteadas en las que personas de filosofía similar combatían durante más de 24 horas en terreno extenso, no en ratoneras.  
Uno, que es por lo natural curioso y escéptico, siempre se ha preguntado en qué se basaban -y se basan- para calcular los precios. La misma curiosidad que experimento respecto a los "patrocinios", los "sorteítos" y demás, mas eso es cosa a tratar otro día.  
Los "expertos" en miljim se esfuerzan, desde tiempos inmemoriales, en diferenciar el *milsim* de las *dominguitas*. En realidad pareciera que el objeto primordial de esta diferenciación es de índole personal, puede que un mecanismo de refuerzo de maltrechas autoestimas, qué se yo.  
Mas de un tiempo acá la incoherencia se hace más patente dado que ya es generalizada la asemejanza total del milsim con la tontamente denostada pachanga de domingo. Bobos solemnes con ínfulas. Empezando por el planteamiento económico, basado en el cuento de la lechera.  
Por más que se empeñen en diferenciarse de la dominguita, empezando por el precio todo son rasgos comunes.  
La variable "tiempo" NO es interpretada de forma cualitativa, sino CUANTITATIVA. De ahí que hablemos de "kilos" de pienso directo al comedero, "precio por kilo".  

<a href="/assets/img/2020-07-11-contabilidad_lechera_miljim/cerdos_comedero.jpg" target="_blank"><img src="/assets/img/2020-07-11-contabilidad_lechera_miljim/cerdos_comedero.jpg" alt="miljim por kilos directa al comedero" width="250"/></a>  
<small>(*) click para ampliar</small>  


- ¿Cómo calcula hoy un "organizador" el precio al que va a vender el kilo de partidica miljim?
- ¿Cómo el subconsciente de aquellos que son "clientes potenciales" -"cualquiera, mientras pague, y cuantos más a hacinar, mejor"- determina si un precio es "justo" o no?  


Muy sencillo, todo se basa en la dominguita, su **PRECIO POR KILO**.  
Una dominguita dura 3 horas mal contadas y su precio oscila entre 10€ y 15€.  
Por consiguiente, el precio por kilo de dominguita es 15€ / 3horas = 5€/kilo  
Y ese es también el precio del kilo de miljim. 24 kilos (horas) de miljim equivale así a 40€.  
Ese es el **precio base** al que el palillero organizador sumará **"los aditivos"**, una especie de impuesto al valor añadido; porque todo el mundo sabe que al igual que el término "táctico", el palabro **"milsim"** otorga un "plus" en el precio.  
No ha mucho se *cortaban* con los pluses en el kilo de miljim, pareciera que les daba apuro. Hoy se hace de forma descarada y la parroquia lo permite e, incluso, profiere a viva voz que el miljim hoy aún es barato en España.  
Evidentemente les mean en la cara y ellos dicen que es lluvia.  
Por eso hoy no es raro encontrar partidicas de esas a **60 pavos / 24 horitas** (que nunca son, porque el término "nonstop" -como ya tratamos en este sitio- es también muy elástico).  

Otro mecanismo, a parte de incrementar el precio/kilo frotando el miljim con cuerno de unicornio, es tener -desde el principio- al personal vagando por el monte en lugar de acampado antes de la partida. Eso equivale a **más kilos de miljim** y, por consiguiente, más dinero. Está bien prescindir de acampadas, origen de molicie y muchos otros vicios, y empezar la operación inmediatamente si el motivo fuera cualitativo no, como es el caso, cuantitativo.  

Y, para finalizar, el **hacinamiento**. Otro recurso de la contabilidad lechera para aumentar esos beneficios que, dado el piruletismo imperante, tal como vengan se irán.  

Está claro que en los subconscientes, lo que hay en esas cabecitas de pájaro, empezando por la percepción distorsionada de "valor" y "precio", no es más que esa misma pachanga o dominguita que tanto denostan (porque ellos son "operadores" de verdad, mejores que "los otros", "los demás", "los dominguiteros" y "espidsofteros", claro que sí) disfrazada de supuesto glamour. Un sepulcro blanqueado en definitiva.  

Una de las repercusiones de la contabilidad lechera es que el dinero fácil tal como viene se va.  
Y por el camino quedan las **trifulcas** internas que, generalmente y al olor de los billetes, las chupipandis de organizadoras mantienen y promueven. La cueva de Alí Babá, trapos sucios lavados en público, prostitutas peleándose por una esquina, agarrándose de los pelos... lucha en el fango.  

Dice un adagio castellano que «sólo un tonto confunde valor con precio» y que «un tonto y su dinero no permanecen mucho tiempo juntos».
