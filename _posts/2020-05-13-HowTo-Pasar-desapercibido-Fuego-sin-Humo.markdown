---
layout: post
title: "[HowTo] Pasar desapercibido: Fuego sin humo"
author: Cowboy DespertaFerro
date: 2020-05-13
category: Técnica
keywords: survivalismo, bushcraft, preparacionismo, Simulación Militar, howto, milsim, mochila, montañismo, echarse al monte, frugalidad, fuego
---
Hoy todo está prohibido.  
Cuando sobre el terreno no queda más remedio que hacer fuego lo fundamental es no provocar un incendio, sí, y lo siguiente es **no ser detectado**. Al respecto, el enemigo primordial es el humo, el olor y la luz. Si no hay apenas humo no habrá apenas olor. Si el fuego se construye de una forma concreta, además no se producirá apenas luz.  
Construir fuego es todo un arte, desde la iniciación, pasando por el mantenimiento y terminando con el apagado. Es voluntad de cada cual complicarse simulando situaciones complejas o sencillas. Iniciar un fuego con ferrocerio es una satisfacción hoy asumible por muy pocos.  

La aparición de **humo** en un fuego depende fundamentalmente de 3 cosas:
- humedad de la madera
- temperatura del fuego / combustión
- técnica de construcción de la fogata (circulación del aire, etc)  

La mayor parte del humo se debe al vapor de agua y a componentes como la resina.  
Una madera seca, si la combustión es óptima -es decir, a alta temperatura-, no produce apenas humo.  
A baja temperatura la madera primero se **deshidrata** y, por consiguiente, desprende **vapor**; luego se **destila**, proceso que conlleva la emanación de diferentes **gases**.  
Conforme aumenta la temperatura la madera comienza a desprender **hidrocarburos inflamables** y arde de una forma **limpia**. Esto es lo que interesa.  
Si construímos una fogata con leña muy seca, sin resina, haciendo que combustione a máxima temperatura, siguiendo además las pautas que a continuación se enuncian, **será muy difícil que sepan que estás ahí**.  

<a href="/assets/img/2020-05-13-HowTo-Pasar-desapercibido-Fuego-sin-Humo/dakota_fuego_02.jpg" target="_blank"><img src="/assets/img/2020-05-13-HowTo-Pasar-desapercibido-Fuego-sin-Humo/dakota_fuego_02.jpg" alt="Esquema de fuego subterráneo Dakota" width="250"/></a>  
<small>(*) click para ampliar</small>  

Si la fogata está bajo la copa de un árbol ésta (la copa) ayudará a esparcir el humo, favorecerá que se diluya enseguida en el aire.  

Si el fuego está construído en un agujero al modo "Dakota" (lo que comúnmente se conoce "fuego Dakota") se mejora el tiro, el sistema es más eficiente y se reduce aún más la producción de humo.  
El método tradicional del fuego Dakota se basa en una cámara subterránea (donde arde el fuego) que se comunica con 2 agujeros: 
- salida de calor y humos
- entrada de aire y de combustible  

Si se le añade un conducto de entrada de aire extra (a parte del de aporte de combustible) encarado al viento el sistema mejora aún más, al estilo de una fragua, y -obviamente- será más fácil obtener altas temperaturas de combustión (y menos humo).  
El aporte de combustible siempre es preferible que no sea ni excesivo ni repentino. Uno a simple vista ha de saber cuándo, en qué cantidad y de qué tamaño hay que aportar la leña.  

<a href="/assets/img/2020-05-13-HowTo-Pasar-desapercibido-Fuego-sin-Humo/fuegodakota_00.jpg" target="_blank"><img src="/assets/img/2020-05-13-HowTo-Pasar-desapercibido-Fuego-sin-Humo/fuegodakota_00_grises.jpg" alt="fuego Dakota" width="250"/></a>  
<small>(*) click para ampliar</small>  

Este sistema es interesante porque es sencillo de construir, evita que el fuego se descontrole y provoque un incendio, es prácticamente inmune al viento, disimula la luz de las llamas, aporta mucho calor, gasta poco combustible y, si éste es el adecuado (véase el principio del artículo), apenas produce humo.  



! Advertencia  
Dicho sea, para empezar, que este contenido -y otros que pudieran encontrarse por aquí- no está redactado para sumisos, manazas o aprendices de Rambo / BearGryls. El fuego se hace cuando no queda más remedio, sabiendo lo que se está haciendo por experiencia y conocimiento. Los experimentos han de ser con gaseosa. Dado el panorama liberticida existente en mi amado país, donde no se puede hacer nada sin permiso, considero un deber moral DESOBEDECER.
