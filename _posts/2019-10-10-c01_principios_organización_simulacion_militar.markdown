---
layout: post
title:  "CAPÍTULO I. Algunos principios fundamentales del universo"
author: Cowboy DespertaFerro
date:   2019-10-10
category: Sátira
keywords: simulación militar, darwinismo, eugenesia
---
BATALLITAS DE MENTIRA Y CHICHA Y NABO.
(VENTURAS Y DESVENTURAS DE UN JUGADOR DE SIMULACIÓN MILITAR)

#### CAPÍTULO I. Algunos principios fundamentales del universo

- - -
En un acto de simulación militar pura y dura, el padrino, que es quien lo organiza, debe tener siempre atados y bien atados los siguientes principios, tan verdaderos como las leyes de la termodinámica. De ello depende el triunfo sublime, o el grotesco fracaso.


##### **Principio número 1**: Somete en todo momento a los supuestos "veteranos" y relaja a los "nobeles".
80% de las personas que acudirán a tí son farsantes, urbanitas hijos del confort, muchos de ellos objetores. Otros son pistolos o policías de barrio... pepitos piscinas. Dicen más de lo que realmente son. Jamás les dés un trato preferente, ni confianzas, ni les permitas creerse algo; fuérzales a que se sientan obligados siempre a demostrar -o demostrarse- algo. La vergüenza o el sentido de ridículo es algo siempre explotable. En el momento en que no experimenten esa sensación se abandonarán a la molicie.


##### **Principio número 2**: exige tanto como eres capaz de dar.
Ejemplaridad es fundamento del liderazgo, liderazgo es respeto y no hay respeto si no se predica con el ejemplo. No quieres corderos ni sumisos.


##### **Principio número 3**: El buen paño en el arca se guarda.
Minoritarismo, es decir, poca gente. Opacidad total hacia el exterior. Al que pía se le manda a un rincón, no vaya a ser que ponga un huevo. Eugenesia.


##### **Principio número 4**: Pon en cada unidad a uno de tu confianza.
Ni siquiera él sabrá que está ahí para eso también. Creerá que está para jugar, que para eso vino, mas predicará con el ejemplo pues tiene la misma filosofía que tú, mantendrá el rebaño en orden y, sin tan siquiera ser consciente de ello, te mantendrá informado antes, durante y después.


##### **Principio número 5**: Controla el botón del _"non stop"_ del radioperador de cada unidad.
Esto merece <a href="https://airsoftmadmaxista.gitlab.io/sátira/2019/10/11/radio_corchopan_explicaciongrafica.html" target="_blank">capítulo a parte</a>. ¿Para qué hablar dello ahora?


##### **Principio número 6**: exponte -tú y los demás- a no más de un 10% de desconocidos.
Nunca más de un 10% de chupipandis que no hayas visto sobre el terreno, en tu propio acto, cómo funcionan.
Recomendados hay muchos, pocos valen lo que realmente te cuentan. Relájalos, no hace falta que te esmeres en que en esta su primera vez se sientan obligados a esforzarse, eso generalmentelo harán por sí solos. Si atas corto al supuesto "veterano", los nobeles harán todo lo que les pidas.


##### **Principio número 7**: No contabilidad lechera.
Estás en esto para crear, no para ganar dinero, poder o amigos nuevos. No seas manirroto. No compres, no vendas, produce. Sé frugal: sólo los tontos confunden **precio** con **valor**. Sólo los incapaces necesitan atrezzo para tapar carencias de imaginación o capacidad de improvisación. El saber por encima del tener.


##### **Principio número 8**: No pidas favores y rechaza patrocinios
Esto es s.m., no necesitas a los hijos del dinero; si aceptas algo dellos, les pertenecerás y, lo que es peor, tu coherencia desaparecerá. Libertad es responsabilidad, sé libre y los que vengan a tí, mientras dure el acto, lo serán también.


##### **Principio número 9**: Selección Natural.
El que la caga no vuelve. No te cases con nadie, todos -incluído tú- sois igual de insignificantes y eso es inmutable. El acto se engendra, nace, crece y muere. Después nada material queda, mas -repito- el que la caga no vuelve, y el que pía... ¿qué hacemos con el piante? Al rincón, por supuesto, no vaya a poner... ¿Qué ponen lo piantes? Los piantes ponen huevos.


##### **Principio número 10**: Nadie viene obligado.
Recuérdaselo cuando fuere menester. Una vez están dentro no hay huída honorable si no es por fuerza mayor.

¡Ten CUIDADO! El acto de simulación militar no tiene por objeto dejar en la cuneta a los participantes. Respeta y te respetarán, sé duro y sé justo, acepta la misma dureza cuando la merezcas. El fracaso de un número significativo de participantes es, por encima de todo, tu propio fracaso. Hiciste mal tu trabajo, vago. Si acontece en gran número se deberá sin lugar a dudas a tu incompetencia. En tal caso has de proceder de la siguiente manera: 1) cierra el pico, 2) regresa a casa, 3) abre la espita del gas, 4) abre la puerta del horno, 5) mete la cabeza dentro (del horno), 6) respira hondo y cuenta hasta mil.


##### **Principio número 11**: Desconfía del que te adula.


##### **Principio número 12**: Los "losientos", "creíques" y "pensequés" son primos hermanos de Don Tontequé.
