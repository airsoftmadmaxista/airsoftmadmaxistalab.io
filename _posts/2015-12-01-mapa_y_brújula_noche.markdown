---
layout: post
title:  "Mapa y Brújula en la noche, o cómo evitar ser un imán de bolas"
author: Cowboy DespertaFerro
date:   2015-12-01
category: Técnica
keywords: airsoft, brújula, combate nocturno, gps, infiltración, mapa, milsim, noche, topografía, ocultación, silencio, carne de cañón
---

> **Qué es la brújula, te preguntas**

— La brújula es ese chisme que queda tan bien en tu "*ekipa*" (equipación, traducido a lenguaje de adultos), aunque  desconozcas cómo usarla.

... o, aún puede ser peor... 

> **¿Donde están los todoterrenos para transportar mi mochila?**

— El todoterreno son tus piernas (...abandonos incoming).


Esto no es nuevo, obviedades traigo. El papel no emite luz. La brújula tampoco.

Si no fuera tan perezoso revisaría los **<a href="/assets/pdf/2015-12-01-mapa_y_brújula_noche/o-0-4-29-combate-nocturno.pdf" target="_blank">manuales de combate nocturno del ejército español</a>**.

<a href="/assets/img/2015-12-01-mapa_y_brújula_noche/luciérnaga.jpg" target="_blank"><img src="/assets/img/2015-12-01-mapa_y_brújula_noche/luciérnaga.jpg" alt="ella desea ser vista, afán de fornicar" width="250"/></a>
(*) click para ampliar

Así, a bote pronto, una serie de consideraciones particulares:

Un mapa y una brújula pueden ser consultados a la luz de las estrellas o la luna. En condiciones de baja visibilidad, una linterna "ciega" puede servir para consultarlos bien parapetado y/o con una chaqueta o lo que sea encima.

Hay brújulas luminiscentes, y si en el mapa perforamos el punto de salida y el punto destino podemos leerlo precariamente -y con buena memoria- incluso al tacto.


No estoy en contra del GPS; es una herramienta muy útil de la que casi todos nos beneficiamos siempre que podemos, pero tienen el inconveniente de que de noche no queda otra que prescindir del "modo sin retroiluminación" (casi imposible de leer) y usar el "modo con retroiluminación" que, aún con ésta al mínimo, es visible a simple vista a un centenar de metros.


Ilustrémoslo

Miljim en camponegocio Serraconya, primavera de 2015. El campo/explotación, un coto de caza muy reshulón, circunstancia que atenuaba la sensación de que la partida no estaba teniendo la "calidad" debida (y pagada). Sin ritmo, con mucho atrezzo y guión -totalmente prescindible-, con un mando/coordinador -de la organización- a cargo de TODAS las unidades de nuestro bando decididamente inexperto cuando no directamente incompetente, etcétera, etcétera...

**Cito textualmente** a un testigo de primera mano:

> **«**Cae la noche, y al caos organizativo inicial (todo el mundo deambulando de aquí para allá, tropezando unos con otros), le sigue la espectativa de refriega en las tinieblas. La noche promete [horas más tarde se desataría un pequeño infierno en forma de rayos, truenos y chuzos de punta, y todos mandados a dormir no se fueran a mojar las réplicas o caerles un rayo XD].

> El jefe de la unidad -airsofter vasco, buen tipo, muy hábil- sugiere entonces que navegue yo. Y heme aquí, en la vanguardia, avanzando junto a 3 airsofters más y **mi gps que -aún con la pantalla en baja intesidad- me servía, más que de guía, de antorcha. Aunque sólo lo encendía lo justo, en aquellos instantes era un imán de bolas en la noche, carne de cañón de libro**.

> Afortunadamente, aquellos del otro bando eran muy dados al abuso del iluminador IR de sus **visiones nocturnas** GEN I "tó reshulonas" y fueron detectados antes de que nos cosieran. Podrían haberse abstenido de usarlos, nos habrían detectado a centenares de metros a simple vista. Aún no era la hora**»**.    — *Uberkannter soldat*
