---
layout: post
title:  "De mamapollas y tontos útiles cuando las explotaciones del airchoft barruntan mermas económicas"
author: Cowboy DespertaFerro
date:   2020-04-30
category: Sátira
keywords: explotaciones del airchoft, empresaurios, mamapollismo, teletienda, compraventa, sentimentalismo, llorones, mendicidad 4.0, contabilidad lechera, quiebra, unión de campitos, darwinismo, eugenesia, palmeros, pesebres
---
<a href="/assets/img/2020-04-30-mamadores-publicitan-campitos-quiebra-covid/explotaciones_airchoft_mendigando_crisis_covid.png" target="_blank"><img src="/assets/img/2020-04-30-mamadores-publicitan-campitos-quiebra-covid/explotaciones_airchoft_mendigando_crisis_covid_bn.png" alt="mamadores-covid" width="250"/></a>  
<small>(*) click para ampliar</small>

Insistieron en pasármelo hace unas semanas, pese a que conocen que formo parte de ese pequeña pandilla de hombres excépticos que están vetados en aquellos lodazales..., ya saben, las teletiendas del airchoft, los pesebres, esa especie de urinarios públicos... el hotel Felación.  
**¡Cómo se estarán viendo algunas €xplotaciones de airchoft de la provincia de Barcelona -aquellas que no ha mucho andaban subiendo precios y haciendo listas de "jugadores indeseables"- que han de mendigar a través de sus MUÑECOS, incidir en la vena sentimental del jugador de base** allá en ese estercolero publicitario llamado Airsoft Barcelona!  
Ah, el sentimentalismo... Emocional y CURSI hasta la arcada en estos tiempos de monotema, *aplausitos* y laminación liberticida.  
Siempre supe que esto estaba lleno de mamapollas. El espectáculo es francamente divertido. Contabilidad lechera, **mendicidad 4.0**.  
Hay dos maneras de perecer: con dignidad o arrastrándose. Uno fenece, trabaja, juega... como vive.  
¿Y ahora qué?  
Sospecho que, entre otras cosas, a parte de episodios propagandísticos de teletienda como el que nos ocupa, para salvar el neg-ocio a corto plazo dirán que, por "motivos humanitarios" bajan precios, harán ofertas y movidas de beneficencia.  
Ya saben, maeses, vds que son aventajados:
- Cuando la **caridad** se publicita, no es caridad sino **márketing**
- El **marchante** que para salvar el negocio pierde la honra, pierde la honra y pierde el negocio. Es inexorable.
