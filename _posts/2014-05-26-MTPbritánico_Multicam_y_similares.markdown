---
layout: post
title:  "MTP británico, MultiCam y similares"
author: Cowboy DespertaFerro
date:   2014-05-26
category: Equipamiento
keywords: equipamiento, equipación, recreación histórica, U.S.A., Gran Bretaña, MTP, multi-terrain pattern, MultiCam, uniformes, uniformidad, surplus, crye precission, multicam británico, Tru-Spec
---

En su día me hice con un uniforme MTP de surplus procedente de Afganistán tirado de precio: hace su trabajo más que dignamente en muchos de los terrenos que podemos encontrar en la Península Ibérica... durante el día.
Esa precisamente es la única pega, porque al caer la noche es demasiado visible para mi gusto.

<a href="http://en.wikipedia.org/wiki/Multi-Terrain_Pattern" target="_blank">MTP (Multi Terrain Pattern)</a> es el equivalente británico al <a href="http://en.wikipedia.org/wiki/MultiCam" target="_blank">multicam</a>. Este último es el que prefiero (y el que, procedente de Afganistán, encuentro más barato en las tiendas surplus).

<a href="/assets/img/2014-05-26-MTPbritánico_Multicam_y_similares/mtp_brit.png" target="_blank"><img src="/assets/img/2014-05-26-MTPbritánico_Multicam_y_similares/mtp_brit_transp.png" alt="MTP británico" width="250"/></a>

(*) click para ampliar


He aquí más información sobre este patrón de camuflaje (extraído de <a href="http://www.universosniperairsoft.com/revisiones/truespecmulticam.htm" target="_blank">universosniperairsoft.com</a>):


##### Introducción #####

El patrón de camuflaje MultiCam, está diseñado para ayudar a la persona que lo viste en su ocultación en gran variedad de entornos, estaciones del año, terrenos y condiciones lumínicas. Fue desarrollado pensando en las necesidades de camuflaje de diferentes entornos, usando una sola prenda de vestir. Mientras hay muchas patrones de camo para ubicaciones específicas, el MultiCam se ha diseñado para trabajar bien a través de un muy amplio espectro de condiciones ambientales cuando se observa tanto en el rango visual y como en espectros del infrarrojo (visión nocturna).

<a href="/assets/img/2014-05-26-MTPbritánico_Multicam_y_similares/800px-Future_Force_Warrior_2007.jpg" target="_blank"><img src="/assets/img/2014-05-26-MTPbritánico_Multicam_y_similares/800px-Future_Force_Warrior_2007.png" alt="MultiCam" width="250"/></a>

(*) click para ampliar


##### ¿Como dicen que trabaja? #####

Toma las sombras del ambiente que le rodea y el patrón es lo suficientemente sutil para reflejar parte de los colores circundantes del ambiente. Toma una apariencia verde general cuando se encuentra en un ambiente verde de bosque y un color amarronado claro cuando se encuentra en desierto abierto. Adaptándose a la variación de condiciones de iluminación locales, el modelo se mezcla bien en muchos ambientes, elevaciones, temporadas, condiciones meteorológicas, y horas del día.

**Disfraza el volumen y la forma**

El diseño aprovecha el modo que el ojo/cerebro humano percibe la forma, el volumen y el color. Usando un sistema de resolución digital de alta densidad, patentado por la firma, los colores presentes se van degradando de un color a otro, así que colores y formas del modelo se hacen difíciles de reconocer y definir. El perfil del usuario que lo viste se rompe y se descolora en cualquier color o forma que lo rodee. Esto trabaja en el principio de que un observador puede ver algo, pero todavía no reconocer en ello una forma familiar. Ya que sólo una muy pequeña parte del ojo humano percibe el color, el cerebro se encarga de hacer de relleno para el ojo. El Multicam aprovecha éste principio y ayuda al observador "a ver" el modelo como parte del fondo.

**Escala de equilibrios y contrastes**

La escala y el contraste de los elementos del modelo, se han diseñado para trabajar bien cuando se le observa tanto de cerca como de lejos. Todos conocemos que la idea es la de romper la forma humana, pero sin un ghillie suit o una roca para ocultarnos detrás, este se normalmente con los contrastes entre los elementos grandes de un patrón. Sin embargo, en los modelos con elementos de contrastes, dejan de funcionar en terrenos abiertos (por ejemplo el árido español actual de tierra, en desiertos abiertos). Este defecto se hace más patente, sobre todo, conforme el observador se va acercando al patrón de camuflaje, que hace que los trazos grandes del mismo se vayan de escala de tamaños con los detalles del fondo que rodea al observador. El Multicam confía más en un efecto de mezcla con el entorno que un efecto de contraste para disfrazar al usuario. Este efecto permite que ello funcione en una amplia variedad de ambientes, y mantiene el patrón efectiva aún en distancias cortas. 


##### ¿Cómo se desarrolló? #####

Poco después de los depliegues de tropas estadounidenses en Afganistán e Irak, donde las tropas vestían los uniformes desérticos de 3 colores o los BDUs woodland de toda al vida, se hizo evidente que se necesitaba un patrón de camuflaje que cubriera a los soldados mejor, como se hizo evidente en el año 1991 en la primera Guerra del Golfo.

Crye, la compañía fabricante, pasó mucho tiempo estudiando cómo trabaja el camuflaje natural, cómo los animales se mezclan con el entorno y cómo los objetos naturales toman los colores que los rodean. Consideró dónde es más difícil esconderse y estudió qué elementos de terreno / elementos ambientales eran comunes al mayor número de ambientes posible. Examinó cómo la luz incidía en los elementos ambientales. También investigó los cambios estacionales y cambios de elevación que afectan cualquier región e intentado factorizar todo esto junto para hacer un modelo adaptable y eficaz. Todas las observaciones y parámetros, se mezclaron en un modelo informático, y se le ofreció al US Army. Se creó entonces el Multicam como patrón multientorno de camuflaje, y en su desarrollo final, Crye trabajo conjuntamente con el U.S. Army Soldier Systems Center (también conocido como el U.S Army Natick Laboratories). Crye Associates entró en el consurso del futuro uniforme del US Army, y cuando se daba al patrón de camo Multicam por ganador, debido a que era muy superior en todos los aspectos a los presentados a consurso, al final el elegido fue el ACU. El MultiCam está presente en el programa del  U.S. Army "Future Force Warrior".

---


Fuentes:

<a href="http://www.universosniperairsoft.com/revisiones/truespecmulticam.htm" target="_blank">UniversoSniperAirsoft</a>
