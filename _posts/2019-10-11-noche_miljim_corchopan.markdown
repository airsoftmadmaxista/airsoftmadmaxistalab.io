---
layout: post
title:  "Una noche cualquiera en milsim de corchopán (en el mejor de los casos)"
author: Cowboy DespertaFerro
date:   2019-10-11
category: Sátira
keywords: miljim, corchopán, molicie, vagancia, narcolepsia, simulación militar
---
— Antonio, ¿les habrá pasado algo?

— *La whiskería cierra a las tres, ¿vienes?*

Son casos prácticos de todo el mundo conocidos.
Comunmente en este tipo de partidicas el que organiza desea cobrar mucho y trabajar poco. La situación le va al pelo.

Y el soldadito de goma, que paradójicamente no ha ido obligado sino por propia voluntad, generalmente ya se ha echado unas fotos e interactuado con otros para tratar de posicionarse como alfa, y al atardecer ya anda desorientado, cansado física y mentalmente y con ganas de irse a su puta casa a ver su serie de nancyseals favorita o Lagrimas del Sol... mierdas así.

- - -

(Mañana se echarán las culpas unos a otros. Y pasado, repetirán)

<a href="/assets/img/2019-10-11-noche_miljim_corchopan/corchopan_adormir.jpg" target="_blank"><img src="/assets/img/2019-10-11-noche_miljim_corchopan/corchopan_adormir.jpg" alt="noche de corchopan" width="250"/></a>

(*) click para ampliar

Lo mismo, mas con visiones nocturnas por almohada.

<a href="/assets/img/2019-10-11-noche_miljim_corchopan/corchopan_adormir_vision nocturna.jpg" target="_blank"><img src="/assets/img/2019-10-11-noche_miljim_corchopan/corchopan_adormir_vision nocturna.jpg" alt="noche de corchopán versión nvg" width="250"/></a>

(*) click para ampliar

Dulces sueños, capones. Hay que estar despierto para el sorteíco.


Horas antes tenían su *brífing*

¿Cómo eztán uztedeeeees?!

— Así empiezan los brífings en el nuevo orden del miljim.

Y luego acontece lo previsible, por supuesto :D.
¿Y después del luego, del fiasco, del atracón de corchopán en lonchas gruesas pagado a precio de jamaro de primera? Lo de siempre: llorar y patalear por las redes sociales para tratar de reforzar un ego maltrecho y las carencias que desde la infancia se arrastran. "Yo no fui, fueron ellos, los otros".

Cuánto chota, cuánta alcahueta y cuanto miljimero de escaparate. Maniobras para intentar desmarcarse de la masa asistente para así, mediante el rollito quejica y acusica, tratar de posicionarse en una esfera superior y trascendente, al margen de la lana con la que compartió acampada, ponys y tarta de manzana.

Trucos viejos. No cuela.



