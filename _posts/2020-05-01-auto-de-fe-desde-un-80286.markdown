---
layout: post
title: "Autos de fe desde un 80286"
author: Cowboy DespertaFerro
date:   2020-05-01
category: Sátira
keywords: explotaciones del airchoft, empresaurios, mamapollismo, teletienda, compraventa, sentimentalismo, llorones, mendicidad 4.0, contabilidad lechera, quiebra, unión de campitos, darwinismo, eugenesia, palmeros, pesebres, web 4.0, postureo, mala praxis, mala fe, tontos, chupipandis, parchecicos y tontería, urbanitas, goretex, chupipandis con ínfulas
---
Una vez le dije sincera y públicamente a uno de esos tontainas, mamador e influencer con ínfulas, que esa tronca (o chorba) a la que sin venir a cuento exhibía por internet como si de doméstica mascota se tratara, «tenía pinta de mujer maltratada». No veas qué pataletas :D Total, por una obviedad... y por exponerse. El precio de la fama, el *chonismo* y las pretensiones.  
<small>**Dada la afectación, sospecho que aún le durará la rabieta... Tanto invocar al diablo que al final éste se acaba presentando, y cuando lo hace ya es para quedarse. Hay gente que no es consciente de estas cosas. Por eso hay que pensar bien lo que uno desea.**</small>  
<br>
Al grano.  
<big>**R**</big>edacto muchas veces desde un procesador 286 del año 1991 sito en la sala de informática del Penal. Wordperfect 5.0 es la herramienta. El teclado es mecánico de los de antes, una gozada. La red local es de cable coaxial; el triásico o jurásico, y es genial.  

Uno ya va conociendo al género humano; sospecho que más de uno (y de dos), si supieran..., estaría tentado y -si se pudiera- dispuesto a abonar el importe, o incluso el doble, o el triple del valor de mercado de esas herramientas. :) Sin armas no hay crimen (¿o era sin cuerpo?). De nada serviría, no obstante; sobra decirlo, ¿verdad, maeses? Es evidente que el que escribe esto es de los que disfrutan con lo que hacen.
  
En estos hobbies del primer mundo tenemos a inconscientes personajes que, aún cagándola, pecan de extrovertidos y sociables (y por tanto, cuando hay carencias, vulnerables son): No la hagan no la teman... y a quién le importa internet y las relaciones sociales que ahí se dan, muchacho.
Seca esas lágrimas, muñeco, no te vayas a poner a llorar ahora, por amor de Dios, que si hay algo que a los seres como yo motiva a la hora de clavar la lanzadera es constatar que lo que digo -que es cierto siempre- causa pataleta y desasosiego.  

Cuando me doy cuenta de que a alguien que ha hecho algo potencialmente vergonzoso o susceptible de mofa y ejemplarizante escarnio le afecta lo que dél digan en este internet 4.0 de la mendicidad y el sepulcro blanqueado no queda otra que continuar con el trabajo de fusta, el auto de fe. Y ahí van los capirotes directos al cadalso.  
<a href="/assets/img/2020-05-01-auto-de-fe-desde-un-80286/auto_de_fe.png" target="_blank"><img src="/assets/img/2020-05-01-auto-de-fe-desde-un-80286/auto_de_fe.png" alt="auto de fe" width="250"/></a>  
<small>(*) click para ampliar</small>  
Nada hay más instructivo que la praxis, lo empírico... el ejemplo.  
Habrá quien pensará o dirá, tanto da, que uno es un desalmado sin corazón... hosco, huraño, asocial, etcétera... Me lo sé de memoria. Negativo, no, no es así, aunque si lo fuera tanto daría. Uno también tiene corazón y sabe callar cuando hay cuestiones que, siendo relativamente futiles, pudieran salpicar a terceras personas por las que, siendo inocentes (por decirlo de una manera, porque llegada la edad adulta nadie es inocente) uno pudiera sentir algo parecido a la empatía. Así ya no me hace gracia. Es el caso de un hombre llamado Johan y la torpe gente que, desafortunadamente, pareciera que come en su plato.  

Érase una vez en Felinolandia... (continuará)  


