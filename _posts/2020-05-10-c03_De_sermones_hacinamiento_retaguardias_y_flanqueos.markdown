---
layout: post
title: "CAPÍTULO III. De sermones, hacinamiento, retaguardias y flanqueos"
author: Cowboy DespertaFerro
date:   2020-05-01
category: Sátira
keywords: postureo, mala fe, tontos, chupipandis, parchecicos y tontería, urbanitas, goretex, chupipandis con ínfulas, sentimentalismo, llorones, darwinismo, eugenesia, radio, visor nocturno, nosinmiaifon, Simulación Militar
---
BATALLITAS DE MENTIRA Y CHICHA Y NABO.
(VENTURAS Y DESVENTURAS DE UN JUGADOR DE SIMULACIÓN MILITAR)

#### CAPÍTULO III. De sermones, hacinamiento, retaguardias y flanqueos
Dejen que les cuente un cuento.
Érase una vez en Felinolandia...

Oscuridad absoluta. Sabemos que están ahí en frente, emboscados en la vegetación y las rocas de aquella vaguada a menos de 500 metros... esperando. Podríase decir que todo va bien, que no son conscientes de nuestra presencia aún, si no fuera porque cierto es también que se dice que estadísticamente un asalto frontal contra una fuerza mayor suele acabar en desastrosa carnicería.  
A ambos lados, desnivel positivo, terreno relativamente escarpado. Nada que no pueda subirse con la esporádica ayuda de las manos a los pies. Dicen que nuestros ancestros andaban a cuatro patas y vivían en los árboles. No me creo nada.  

*Flanqueemos, entretenga un grupo el frente mientras asaltamos por los lados y la retaguardia*, propone un adalid. Bien.  
Hay demasiadas reticencias a trepar a oscuras. Secundamos muchos de inmediato la propuesta y comenzamos a evolucionar por el lado derecho, que era el más propicio; non hay tiempo que perder. El asalto es un suicidio mas lo vamos a acometer de la mejor manera posible. Aún no saben que estamos ahí, ya a doscientos metros encima de lo que atisbamos es su primera línea, avanzando metro a metro, preparando la embestida.  
<br>
<a href="/assets/img/2020-05-10-c03_De_sermones_hacinamiento_retaguardias_y_flanqueos/incursionando_noche.jpg" target="_blank"><img src="/assets/img/2020-05-10-c03_De_sermones_hacinamiento_retaguardias_y_flanqueos/incursionando_noche_grises.jpg" alt="incursionando la noche" width="250"/></a>  
<small>Noches de incursión. ('*')click para ampliar</small>  

En estos menesteres la costumbre que uno tiene asumida es que cada cual ya sabe trepar, que ya es autosuficiente y sabe hacerlo bien, de día o de noche, sin dejar caer peñones y sin hacer demasiado ruido. A 200 metros una roca cayendo es un escándalo, suena como un tiro. Si ocurre, no pasa nada, os han detectado y se acabó el factor sorpresa... asúmelo, porque esto en definitiva es una batallita de mentira, y prepárate para los planes bé y cé, que el lector imaginará cuáles son.  
Ahí, cinco metros arriba, en cuclillas anda el binomio y alguna sombra más. Bien. Uno, triste figura fusil de mentira al hombro, continúa subiendo ufano por la pendiente sin mirar apenas atrás, despreocupado de la comitiva, ocupándose tan sólo de discernir dónde va a colocar manos y pies en el siguiente paso, los sitios por donde va a ir escurriendo el bulto y la línea imaginaria que va a ir trazando hasta el punto que considera más oportuno para posteriormente lanzarse a castigar flancos o retaguardia. El cuerpo más cerca del suelo que del cielo.  
Desconocía que el personaje estaba tan siquiera ahí atrás. Le delató su voz. Si uno va a mascullar algo peor que el silencio debería tener la lengua quieta. Hube de dejar de trabajar para atenderle.  
-- *Oye, apaga la radio, que nos van a detectar* -resopló. Era unos de los peleles que muy probablemente antes eran reticentes a trepar.  
Agarrado a unas matas, sorprendido eché la vista atrás para ver a una silueta a escaso medio metro de mi grupa. Mi gen de hijoputa acababa de despertarse definitivamente.  
-- A doscientos metros, en estas alturas, nadie oye esta radio. -bufé, y aún callé y contuve el "Desconozco qué quieres, muñeco, mas nadie te pidió que me siguieras tan pegadito a mis talones. Intenta no molestar o échate a un lado" que resonaba como un cañonazo en mi fuero interno.  
Oyó la radio porque lo tenía pegadito a mí, pareciera que siguiéndome ni que fuera un perrillo faldero. ¿Pisar mi huella, que no soy ni el más útil ni el más sociable? Si me hubiera tirado un pedo hubiera notado el aire en la cara. Interrupciones para intercambiar saliva y tomarse las confianzas que jamás doy porque sí. Sin contar con que la cercanía en las trepadas no es buena por demasiadas razones; si el muchacho perdiera pie y me tuviera a su alcance, sin duda se me hubiera agarrado al pernil y ambos hubiéramos rodado metros ladera abajo. Demasiado cerca, aún no comprendo por qué (¿quizás por miedo a la oscuridad?), tanto que si caigo -cosa nada improbable y sí muy propia- lo hago rodar también cuesta abajo.  

¿Por qué la gente se empeña en hacinarse?  

Llevar auriculares en una radio cuando uno campotravés escudriña las tinieblas que tiene frente y es el oído lo que, a oscuras, le debería indicar qué pasa alrededor, es un error.  
La radio se lleva entonces abierta como un susurro, sin lamparitas ni mierdas típicas de Baofeng. Eso cualquiera debería saberlo, y el que no lo sepa que pregunte por ahí que para eso tiene boca o, mejor aún, que experimente lo que debió hacer de chico. Te has separado del trozo de la unidad que hará de cebo en su momento, estás tan lejos del enemigo como dellos y necesitas saber qué pasa y, llegado el caso, hacérselo saber. El combate no ha comenzado aún, pronto habrá que hacerlo. Puedes apagar el cacharro, mas no por sinrazones y tontería. Dicen que el sentido común es el menos común de los sentidos.  
Cuando trepas, si te da por seguir huella porque no sabes ni imaginas por dónde tirar, dale distancia al que te precede y céntrate en trabajar, en mirar dónde pones pies y manos, sin molestar. No tengas prisa, la desesperación es un arma tan potente como los cañones.  
Lo curioso de los momentos previos a la refriega es que a un tonto lo puedes maltratar que la tensión hará que aún siga perseguiéndote. Porque no sabe qué hacer. Porque anda peor que tú por el monte, más aún a oscuras. Porque esta afición suya por lo bravío es, no ha duda, vana, sin cimiento firme en la tierna infancia. Tú lo sabes y no puedes más que preguntarte ¿qué hace este tipo aquí?  
Tratan de reforzar sus carencias cuestionando lo que ni saben ni están en disposición de saber. Les puede las ganas de llamar la atención, reforzar un ego que saben maltrecho, tratar de impresionar a desconocidos aún sabiendo que su existencia les importa una puta mierda.  

Todo un nini. Creo que así le llamaban. Teniendo en su entorno de acogida a combatientes de primera como referencia no me explico cómo ese muchacho era aún tan ignorante. Sin duda merecía un correctivo. Nunca les dije nada, ¿para qué? Mejor dejar que la gente se estrelle.  

Y es que hacía horas que teníamos -yo y mi compadre- un mal presentimiento. Ya antes habíamos visto cosas curiosas en la forma de desenvolverse de una parte de la unidad que no nos gustaban nada. Trayectos por pista, todos apiñados, hacinados, tropezando... Confiábamos, no obstante, en la vanguardia, en los que de ella se ocupaban, y en algunos de los que se movían correctamente en el grueso del pelotón. Voluntarios instantáneos fuimos para formar y guardar la retaguardia (esa gran desconocida que nadie quiere), alejarnos veinte, treinta o cuarenta metros de aquel imán de emboscadas, que, de ser, sería seguro por los lados y por atrás. Y aún así hubo que graciosamente replicarle a un señor que de día gustaba de hacerse fotos con una bota metida en un charco como si estuviera vadeando un río. Llegó resoplando :D para espetarnos excitado *que nos acercáramos más al grueso del pelotón*.  
--Negativo, no. Vuelve, hombre, con el pelotón, que esto se hace así. Para la próxima comunicamos por radio, no hace falta que bajes y subas.  
La noche, dicen los manuales y la experiencia así lo corrobora, es "sobrecogedora". Esto muchos lo aprendimos de chicos en la estepa hispana, en cordilleras, cauces, tajos y veredas. Acontece el temor al extravío, la desconfianza hacia cualquier sombra, cualquier cosa o rumor adquiere forma irreal y pareciera una amenaza... y aquellos cuyos cuerpos y mentes aún no pertenecen a la intemperie en todas sus facetas acostumbran a reforzarse aproximándose unos a otros más de lo aconsejable, mascullando entre sí palabras, muchas veces inteligibles, avanzando a trompicones, tropezando con las piedras, unos con otros, por la vereda... siempre por la vereda. Paraíso emboscada, bahía desastre.  

Al turrón, a lo que aconteció en aquel asalto.  

Fuimos detectados en cotas altas, no por inaudibles murmullos de radio, sino por desprendimientos de rocas. No pasa absolutamente nada, lo bueno de la vida es vivirla, lo bueno del combate es combatir hasta el último hombre, gánese o piérdase, mirar de hacerlo lo mejor posible por respeto a uno mismo y a los que en ese momento respiran tu mismo aire perdiendo el tiempo igual que tú en batallitas de mentira. Hay que ser proactivo, decía el jefecillo de una escuadra en la que brevemente permanecí hace mucho tiempo. Era bobo de solemnidad, pero hasta de los tontos se aprende.  

Fuego graneado. Minutos más tarde mientras nuestros compañeros batían el frente y nosotros, ya a toda prisa descendidos, asaltábamos a trancas y barrancas el flanco, arrastrándome por un canal seco sucumbí por fuego amigo. Era el antaño en los tajos sermoneador y su visión nocturna, que también -ambos, fraile y visión nocturna, inseparables- habían bajado de las alturas. No importa, un fallo lo tiene cualquiera. Lo he hecho decenas de veces; ante la duda, aprieta el gatillo sin vacilar aunque te conviertan en el enemigo público número uno.  

Muchas horas más tarde, ya con el Sol luciendo en su cénit, asaltábamos otra posición enemiga y aquel muchacho también nos causó problemas. Un tipo del bando contario, con la leyenda "Tuno Negro" estampada en el pecho (sería su nombre o algo), andaba -ya cadáver- contando las ráfagas del gachó y cizañeaba con nosequé del límite de munición. A callar, pienso y callo, me suda la polla, yo no soy policía y doy por hecho de que aquí somos todos honrados.
Mas no vayan a pensar que sólo este se comportaba como si estuviera en plena dominguita, no. En una de las escuadras que componían nuestra unidad había otro gallo que hubo de obsequiarnos a todos los allí presentes con un bochornoso espectáculo de bacileo verbal más propio de la puerta de una discoteca que de una operación de Simulación Militar de altos vuelos. Coleguita del experto en retaguardias.  

Luego andaban algunos mendigando agua, y más cosas... mas eso es otra historia, otro cuento. Aquello estaba sentenciado al fracaso. Y así fue. Esa jornada concluí no volver.  

Habrá quien crea advertir detestable **SOBERBIA** en estas palabras mías. Suele ocurrir a menudo cuando uno narra lo que acontece en una de las tantas ocasiones en que uno ha de mirar cara a cara al abismo de la tontería... ese mismo abismo que en realidad es el que le mira fijamente a uno, a los ojos, sobrecogedor, penetrante y contumaz. Créase.
