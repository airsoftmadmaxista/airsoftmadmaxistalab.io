---
layout: post
title:  "[HowTo] Ajuste de la mochila y distribución de las cargas"
author: Cowboy DespertaFerro
date:   2014-04-11
category: Técnica
keywords: airsoft, howto, milsim, mochila, montañismo, echarse al monte, frugalidad
---

A la hora de hacer una salida a la montaña todo el mundo sabe que se deben seguir una serie de consideraciones para que la actividad sea lo más satisfactoria posible.
Hemos de tener en cuenta la meteorología ya que de ella va a depender las prendas que llevemos; aún así siempre es conveniente llevar una prenda cortavientos ligera en el interior de nuestra mochila.
Si la actividad va a ser un trekking de varios días, dependiendo de lo exquisito del porteador, en la mochila podríase incorporar más peso, como puede ser saco de dormir, tienda ligera o funda vivac o plástico, hornillo, comida… Un buen consejo es tratar de andar lo más ligero posible para disfrutar de la actividad al máximo.

<img src="/assets/img/0/reclutacowboy.jpg" alt="recluta Cowboy" width="250"/>

A la hora de cargar la mochila con los diferentes bultos que irán en su interior es de sentido común tener en cuenta algunas cosillas:

1. El peso que llevemos en su interior lo vamos a soportar desde dos puntos de nuestro cuerpo, la **cadera** y los **hombros**.
2. Nuestro **centro de gravedad** se encuentra a la altura de nuestro **ombligo** por lo que todo peso que se aleje de el hará palanca en nuestro cuerpo.

Por estos dos motivos la mayor cantidad del peso que carguemos habría de estar lo más cerca posible de nuestro centro de gravedad por lo que los elementos más pesados deben ir pegados a la espalda de la mochila y aquellas cosas más ligeras en su parte externa.
Además los bultos mayores irán abajo para facilitar el acceso a las cosas de menor tamaño en la parte superior donde el acceso es más fácil.
De esta manera se genera un esquema en el interior de nuestra mochila como el que vemos a continuación:

<img src="/assets/img/2014-04-11-ajuste_mochila/distribucion_de_las_cargas_en_la_mochila.png" alt="Distribución de las cargas en la mochila" width="250"/>
*Distribución de la carga en una mochila*

Si cargamos la mayor cantidad de peso en el fondo de la mochila éste realizaría palanca sobre el cuerpo cargando de los hombros lo que haría que nos tirara el cuerpo hacia atrás. Si quisiéramos reequilibrar el cuerpo deberíamos echar el cuerpo muy hacia delante.
Cuando el terreno no tiene dificultad podemos subir la carga un poco más en nuestra mochila, siempre pegado a su espalda, de esta manera, al estar la mayor cantidad de peso justo sobre la vertical de nuestro centro de gravedad nos ayudaría a tener una posición más erguida y natural.

Por otro lado, el modo correcto de colocarse una mochila es el siguiente:

En primer lugar aflojaremos las tiras que rodean nuestros hombros de manera que queden muy holgadas.

En segundo lugar nos ceñiremos el cinturón lumbar entorno a las caderas, de esta manera nos aseguramos que la mayor cantidad del peso de la mochila lo va a soportar nuestra cadera que es una articulación mucho más fuerte y más próxima a nuestro centro de gravedad que los hombros.

A continuación ajustaremos las tiras de los hombros para aproximar la carga a nuestra espalda pero tratando de que estos no soporten peso. Estas tiras únicamente deberán realizar la función de soporte y estabilizado de la mochila para evitar que se mueva pero lo ideal sería que todo el peso recayese sobre nuestra cadera.

Una vez realizado este paso ajustamos las correas superiores para aproximar el eso de la parte superior a nuestro cuerpo y evitar que tire de nosotros.

Por último debemos ajustar el cinturón pectoral que evitará que las tiras de los hombros tiren de estos hacia atrás.
............
Para complementar este artículo he aquí un video explicativo:
<http://www.youtube.com/watch?v=cM37cQrWruU>


----
Fuentes:
	[Francisco Hernández, España Supervivencia Extrema](https://www.facebook.com/pages/Espa%C3%B1a-Supervivencia-Extrema/242739209133716?ref=stream)

