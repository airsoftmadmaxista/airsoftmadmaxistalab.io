---
layout: post
title:  "La doctrina de la guerra relámpago (blitzkrieg)"
author: Cowboy DespertaFerro
date:   2014-05-23
category: Técnica
keywords: Alemania, heer, blitzkrieg, estrategia, formación, II G.M., táctica
---

El General Heinz Guderian, fue el artífice de la estrategia que catapultaría al carro de combate como arma de guerra: la Blitzkrieg o "Guerra Relámpago". Los principios fundamentales de esa estrategia fueron enunciados por Federico el Grande de Prusia y revisados, posteriormente, por Von Clausewitz, Von Moltke y Von Schlieffen.

Por otra parte, Guderian estudiaría las obras de teóricos defensores de la guerra acorazada, destacando Basil Liddell Hart y el coronel Fuller, ambos británicos. Sin embargo, al contrario de lo que se cree, Guderian seguiría las teorías de Fuller, no de Liddell Hart.

La Blitzkrieg se basaba en dos conceptos: El primer objetivo sería la **destrucción de las fuerzas enemigas** y el método empleado sería la **MOVILIDAD**: clave para el desarrollo de esta estrategia fueron, por consiguiente los carros de combate. La destrucción de las fuerzas enemigas, se consigue envolviéndolas (Kesselschlacht) por los flancos, haciendo que sea imposible para éstas reaccionar o escapar.

La primera fase era la **concentración de fuerzas**. El comandante, elegía un "punto clave", para concentrar todas las fuerzas y así, **romper el frente**. Acto seguido -segunda fase- se persigue en profundidad al enemigo y se le **rodea**, sin que éste tenga posibilidad de maniobra.

<a href="/assets/img/2014-05-23-doctrina_blitzkrieg/blitzkrieg.gif" target="_blank"><img src="/assets/img/2014-05-23-doctrina_blitzkrieg/blitzkrieg.png" alt="Esquema del mecanismo de la guerra relámpago" width="250"/></a>
(*) click para ampliar


---


Fuentes:


<a href="http://es.wikipedia.org/wiki/Blitzkrieg" target="_blank">Blitzkrieg en Wikipedia</a>


<a href="http://www.lasegundaguerra.com/viewtopic.php?t=10089&mobile=on" target="_blank">Whitman en lasegundaguerra.com</a>
