---
layout: post
title: "Yo he visto cosas que muchos de vosotros jamás creeríais, vol. II"
author: Cowboy DespertaFerro
date:   2020-05-10
category: Sátira
keywords: chupipandis, parchecicos y tontería, urbanitas, goretex, chupipandis con ínfulas, postureo, explotaciones del airchoft, empresaurios, mamapollismo, teletienda, compraventa, sentimentalismo, llorones, mendicidad 4.0, contabilidad lechera, quiebra, unión de campitos, darwinismo, eugenesia, palmeros, pesebres, web 4.0, mala praxis, mala fe, tontos, neveros orinados, arroyos de orines, youtube, instagram, fotitos, ignorancia, miljim, minimiljim, aguas fecales
---
Yo he visto cosas que muchos de vosotros jamás creeríais.  
Vicio y egos en llamas más allá de Serraconya, Barrancones y Youtubelandia, infectando Simulación Militar como un virus de allende aliexprés, comprando voluntades.  
Personajes mediáticos abalanzarse sobre el hilo de agua de un arroyo para hacerse fotografiar con un pie dentro dél, uno sólo mas el otro no cabía. Armas de mentira vadeando el caudaloso Ebro. Bendito Goretex.  
Ojos brillar en la penumbra de un casco de skater entreteniéndose, pese al inminente combate, en buscar el cuadro adecuado para que en una umbría el único metro cuadrado de nieve remanente en kilómetros a la redonda apareciese en una fotografía ocupándola entera como si de Siberia se tratara.  
Todos esos momentos se perderán en el tiempo como lágrimas en la lluvia. Es hora de morir.  
<br>
<a href="/assets/img/2020-05-10-yo-he-visto-cosas-que-no-creeriais-vol2/charquitos.png" target="_blank"><img src="/assets/img/2020-05-10-yo-he-visto-cosas-que-no-creeriais-vol2/charquitos_grises.png" alt="charquitos" width="250"/></a>  
<small>Aprender cómo funciona la retaguardia de una columna es -no ha duda- el menor de sus problemas, aún hoy. (*) click para ampliar</small>  
