---
layout: post
title:  "Vietnam: Equipamiento 173ª Brigada Aerotransportada (Indep.)DAK TO"
author: Cowboy DespertaFerro
date:   2014-05-25
category: Equipamiento
keywords: equipamiento, equipación, recreación histórica, U.S.A., uniformes, uniformidad, Vietnam, A.L.I.C.E., correajes
---

<a href="/assets/img/2014-05-25-Vietnam_Equipamiento_173_Brigada_Aerotransportada_(Indep.)DAK_TO/EQUIPO_173ª_Brigada_Aerotransportada_(Indep.)DAK_TO.jpg" target="_blank"><img src="/assets/img/2014-05-25-Vietnam_Equipamiento_173_Brigada_Aerotransportada_(Indep.)DAK_TO/EQUIPO_173ª_Brigada_Aerotransportada_(Indep.)DAK_TO.png" alt="Esquema del mecanismo de la guerra relámpago" width="250"/></a>
(*) click para ampliar


2) **TROPICAL COMBAT TROUSERS 3rd PATTERN**: Los laboratorios de Natick diseñaron en 1962 un uniforme tropical de combate en 1962 a instancias del mando del ejército, que solicitaba que se confeccionara un uniforme más acorde con las condiciones de lucha en Vietnam. El pantalón estaba confeccionado en algodón tipo *OG-107* y *Propelina* (material antidesgarro) (*RIP STOP*) en color verde oliva. El pantalón tenía 6 bolsillos: 2 en la parte posterior, 2 laterales y otros 2 en los costados a la altura de las caderas.



3) **TROPICAL COMBAT JACKET 3rd PATTERN**. La Chaqueta perteneciente al 3er. modelo tropical estaba confeccionada en algodón tipo *OG-107* y *Propelina* (material antidesgarro) (*RIP STOP*) en color verde oliva. Llevaba 4 bolsillos, 2 en el pecho inclinados y 2 frontales en la parte baja. Los botones estaban protegidos, a diferencia del primer modelo utilizado en la guerra, que tenía los bolsillos exteriores así como hombreras (los soldados pronto se quejaron a sus mandos,ya que tanto los botones como las hombreras se enganchaban con la maleza en la densa jungla).



4) **OLIVE GREEN T-SHIRT**: La camiseta interior reglamentaria del soldado en Vietnam. Era de algodón, a elegir entre dos colores: blanco y verde oliva *tipo 109*. A cada hombre se le entregaban cinco camisetas y cinco pares de boxers a su llegada en Vietnam



5) **1/4 CANTEEN**: La cantimplora de 1Q fabricada en polietileno y de color verde oliva entró en uso en Vietnam el 14 de Septiembre de 1962. En su parte posterior tiene grabadas las letras US y la fecha de fabricación. Esta cantimplora sustituye al modelo 1910 de aluminio. Acompañaba a la cantimplora un cazo de aluminio. Todo el conjunto era transportado dentro de una funda de algodón denominada *Cover water canteen M1956* que se fijaba al cinturón por medio de dos fasteners metálicos



6) **TROPICAL RUCKSAC**: En 1965 el MACV solicitó a los laboratorios de Natick una versión de nylon de la mochila del ARVN. El 2 de Diciembre de ese mismo año los laboratorios de Natick mandan a Vietnam cuatro ejemplares de un prototipo para su estudio. Del prototipo escogido se encargan 500 unidades que son entregadas el 10 de Mayo de 1967 a los miembros del 5º de Fuerzas Especiales para su evaluación en combate. Una vez obtenidos los resultados, se hace modificar los ángulos de la armadura y se refuerza la mochila en sus extremos. El 4 de Marzo de 1968 se entrega al ejército la mochila definitiva, que está confeccionada en nylon impermeable de color verde *OG106*, es 10 cm. mas grande que la versión del ARVN, y permite añadirle un compartimento exterior mas (total tres) y un gran compartimiento central que tiene un forro para proteger lo colocado en su interior. Como soporte finalmente se le adaptó una armadura en "X". El peso en vacío era de 1,6 kg.



7) **PONCHO LINER - ERDL CAMOUFLAGE**: En 1965 la USARV ordena la confección de un nuevo poncho más ligero que su predecesor y más acorde con el clima tropical. El 9 de septiembre de 1966 se hace entrega de 200 ejemplares de un nuevo poncho ligero (*lightweight*), que pesa 681 gramos y está formado por un acolchado de poliéster intercalado entre dos paneles de nylon en camuflaje tipo *ERDL*. La primera versión es modificada en junio de 1967, y la versión definitiva se confecciona el 8 de abril de 1968 en grandes cantidades y en tela *RIP STOP* (antidesgarro). Se hicieron en dos tallas: 2,33 x 1,67 mts. (para los americanos) y 2,08 x 1,52 (para los sudvietnamitas). Al final de la guerra se realizó una última versión en tela modelo *ERDL48*.



8) **PISTOLA COLT 1911A1**: Semiautomática. Calibre: 45 ACP (11,43mm), peso: 1.105 gramos, Alimentación: Cargador de 7 disparos. Se suministraba una pistolera tipo sobaquera de uso común entre los pilotos, miembros de las fuerzas especiales y algunos oficiales del ejército. Según orden de 1963 en el teatro de Vietnam todas las pistoleras tenían que ser de color negro. reemplazando el color marrón en uso desde la 2GM. Muchas de las pistoleras fueron teñidas hasta la aparición de las reglamentarias fabricadas apartir de 1963.



9) **FUSIL COLT M16 A1**: Calibre: 223(5,56mm), velocidad de tiro: 700/900 disparos por minuto, alcance máximo:460 mts., alimentación: cargador de 20 o 30 disparos, longitud total:990 mm., peso sin cargador: 2,94 kg.

---


Fuentes:

<a href="http://www.museodevietnam.com/archivo/equipo/equipo_infanteria_us/index.php" target="_blank">museodevietnam</a>
