---
layout: post
title: "Hay que *cerrarse caminos* más"
author: Cowboy DespertaFerro
date: 2020-07-03
category: Sátira
keywords: miljim, contabilidad lechera, despilfarro, plutocracia, avaricia, corchopán, simulación militar
---
La mentalidad de empresaurio de las bolitas es muy curiosa, tanto que pareciera les gusta ser blanco de mofa.  

<big>**«Si no me compra el producto, si no obtengo servidumbre, se cierra el camino»**</big>.  
Y lo susurra ufano, con engolada voz, sin ruborizarse siquiera, a conocidos y profanos.  
Jolgorio, festival del humor en el subsuelo.  

Pareciera como si al marchante común de las bolitas y/o eventos le doliera la indiferencia, el sudar de sus rollos. ¿Cómo se puede exponer un tipo a este ridículo? ¿No hay autoestima, dignidad?  
Para hideputas como nos -tan insignificantes como cualquiera- lo bueno de esto de las bolitas es que podemos contemplar la indigencia en su máximo explendor sin tener que encender la tele. Jamás dejaremos este hobbie.  

¿Se imaginan, por ejemplo, al fabricante de Nutella murmurando a tus conocidos que como no te interesas ni compras su cacao estás "baneado", proscrito o algo? :D  
Suena ridículo.  

Y es que desde otoño de 2008 sentenciamos, purgamos y decidimos por aquí no consumir Nutella -manjar de cuerpoescombros-, ni, sobre todo, PARTIDICAS que ya andaban tomando un sesgo corchopánico absurdo.
Somos gente ruda y alegre, franca y sincera.  

<a href="/assets/img/2020-07-03-cerrarse_caminos_proscrito/manifiesto_diciembre2018_arresto.jpg" target="_blank"><img src="/assets/img/2020-07-03-cerrarse_caminos_proscrito/manifiesto_diciembre2018_arresto.jpg" alt="manifiesto diciembre 2018" width="250"/></a>  
<small>(*) click para ampliar</small>  

Es evidente, que nos la suda bastante los "caminos cerrados". Aquí sabemos saltar parapetos. No se le puede poner puertas al campo, maeses :D  

Vaya problema... y mi dinero en mi bolsillo, mejor lugar non ha.  
Y mi tiempo, ese bien preciado por escaso, en mi haber, que es donde ha de estar.  
Nuestra religión, que es de hombres honrados, nos prohíbe financiarles los vicios a naiden. Búsquense un trabajo de verdad.  
