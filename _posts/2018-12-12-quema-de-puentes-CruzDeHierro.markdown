---
layout: post
title: "Quema de puentes"
author: Cowboy DespertaFerro
date: 2018-12-12
category: Sátira
keywords: chupipandis, parchecicos y tontería, urbanitas, chupipandis con ínfulas, postureo, explotaciones del airchoft, empresaurios, mamapollismo, teletienda, compraventa, sentimentalismo, llorones, mendicidad 4.0, contabilidad lechera, quiebra, unión de campitos, darwinismo, eugenesia, palmeros, pesebres, web 4.0, mala praxis, mala fe, web4.0, organizadoresm, miljim, Simulación Militar, quema de puentes
---
Desde tiempo antes de la campaña de 2018, con todo el panorama y todo el excremento de esto de las bolitas bien contemplado y analizado, andaba rumiando no volver a involucrarme ni tan siquiera en esa operación, mi favorita, aquella que siempre consideré la madre de la Simulación Militar ibérica.  
Durante y después, la sensación de alarma se fortaleció. Observamos más cosas -conocidas algunas dellas, sospechadas otras- mas casualmente ya, en esta ocasión, todas juntas. No era que un empresario ganase dinero haciendo simulación militar, en absoluto. El trabajo bien hecho ha de ser recompensado (y constantemente auditado, no vaya a torcerse). Era la irrupción de la molicie, de costumbres nunca vistas en los jugadores y una alarmante predisposición a la arbitrariedad, al caos y acomodo a las tendencias del sector.  
Por primera vez constaté la irrupción de costumbres tan impropias del espíritu de la Simulación Militar pura y sincera, jamás presenciadas antaño, veneno tan propio de otras parroquias. Y las protagonizaban algunos nuevos y también algún experimentados. Costumbres traídas del milsim de corchopán.  
Llevo tiempo observando la evolución de "la empresa" mientras su invento crece. Algún episodio de tirar la piedra y esconder la mano o tratar de agradar a todo el mundo, o lo que es peor, a las princesas del internet. Y eso no puede ser. Un asunto se va al garete así, muere de éxito. Si hay algo que siempre prevalece es la excelencia, la convicción firme e inmutable en la linea determinada en la fundación del invento.  
A día de hoy, 12 de diciembre del año 2018 de Nuestro Señor Jesucristo, hacedor de todas las cosas, con el panorama ya bien trillado y desmenuzado, concluyo con pesar que -de ser fundamentadas mis conclusiones- Desperta Ferro no volverá a involucrarse en La Operación.  
Los derroteros, el destino de la escuadra, permanecen inmutables, y por eso, con la convicción de que empecinarse en hacer lo que considero correcto es el buen camino, éste transcurre, ya sí, lejos de esas campañas y de buena parte de su entorno.  
Del otro sector, el miljim y/o el nuevo orden de las bolicas, pues ya todos saben la postura desde el principio, y nuestra visión sobre esas cosas, obviamente, no cambia tampoco.
