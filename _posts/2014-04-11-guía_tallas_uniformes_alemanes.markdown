---
layout: post
title:  "Guía de tallas de uniformes alemanes actuales"
author: Cowboy DespertaFerro
date:   2014-04-11
category: Equipamiento
keywords: airsoft, alemán, Alemania, Army Surplus, bundeswehr, equipación, flecktarn, surplus, tallajes, tallas, uniformes, uniformes Alemania, uniformidad, howto
---

A veces resulta complicado distinguir con certeza el tallaje de los uniformes surplus (originales) extranjeros. Conocer la talla adecuada es vital, sobre todo cuando se compran por internet.

Esta tabla permite entender un poco cómo funcionan las tallas en los  uniformes del bundeswehr para así poder elegir mejor la talla.

<a href="/assets/img/2014-04-11-guía_tallas_uniformes_alemanes/tallas_bw.png" target="_blank"><img src="/assets/img/2014-04-11-guía_tallas_uniformes_alemanes/tallas_bw.png" alt="Tabla de tallas de uniformes alemanes actuales" width="250"/></a>
(*) click para ampliar la tabla


---
Fuentes:
	<a href="http://www.armysurplus.es/es/tallajes-surplus" target="_blank">Army Surplus</a>
