---
layout: post
title:  "Cañones internos: corte AEG vs. corte VSR"
author: Cowboy DespertaFerro
date:   2014-10-08
category: Tecnología
keywords: aeg, airsoft, cañón, cañón interno, corte AEG, corte M14, corte VSR, mecánica, réplica, vsr
---

Cuando hablamos de réplicas de airsoft y sus cañones internos, el "**corte**" se refiere a la hendidura en la que encaja la goma del hop up en dicho cañón.


Hay 3 tipos de cortes de cañón: **corte AEG** y **corte VSR**, izquierda y derecha en la foto respectivamente, y también el **corte de M14**.

<a href="/assets/img/2014-10-08-Cañones_internos_corte_AEG_VSR/corte_aeg_vsr.jpg" target="_blank"><img src="/assets/img/2014-10-08-Cañones_internos_corte_AEG_VSR/corte_aeg_vsr_bn.jpg" alt="corte AEG (izquierda), corte VSR (derecha)" width="250"/></a>

corte AEG (izquierda), corte VSR (derecha)
(*) click para ampliar

El cañón de corte M14 requiere gomas convencionales de AEG, pero la hendidura para fijar el cañón (donde encaja la pieza con forma de "U") está más cercana al agujero -queda más retrasada- que en un cañón AEG normal. Por eso, para la cámara de Hop Up de un M14 convencional se necesitará un cañón especial (de corte M14) o uno "polivalente" (con muescas a la distancia normal y a la distancia específica del M14).

Un cañón polivalente permite ser montado en M14 -por ejemplo de Cyma o Tokyo Marui- y en cualquier otra AEG que utilice cañones corte AEG.

<a href="/assets/img/2014-10-08-Cañones_internos_corte_AEG_VSR/cañon_hibrido.jpg" target="_blank"><img src="/assets/img/2014-10-08-Cañones_internos_corte_AEG_VSR/cañon_hibrido_transp.png" alt="cañón híbrido" width="250"/></a>

Este cañón interno es híbrido: tiene dos hendiduras paralelas. La que esta mas alejada es la de AEG normal, la anterior es la especifica del M14
(*) click para ampliar

El tipo de cañón que elijamos afecta a:

- Tipo de cámara de Hop Up
- Tipo de goma de Hop Up


Hay **cámaras** de Hop Up y sus correspondientes **gomas** específicas para ambos tipos de cañón.


Por ejemplo, el Bar-10 de Jing Gong -clon de VSR-10 de Tokyo Marui- lleva un cañón interno de corte VSR, con lo que goma y cámara de Hop Up son para ese tipo de cañón interno.

Sin embargo si upgradeamos, por ejemplo, la cámara de hop-up de serie por una cámara PDI deberemos cambiar también cañón interno y goma.

¿Por qué?

Porque la cámara PDI para el Bar-10 o el VSR-10 es para cañones de corte AEG y gomas AEG.



Asimismo generalmente las EBB (electric blow back), llevan corte AEG y las GBB (gas blow back), corte VSR (al menos las de We).
