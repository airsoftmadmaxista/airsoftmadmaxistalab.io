---
layout: post
title:  "Modificar y asegurar el 'pinganillo' que Puxing o Pofung/Baofeng trae de serie"
author: Cowboy DespertaFerro
date:   2020-03-31
category: Técnica
keywords: simulación militar, radio, pmr, dmr, hack, tecnología, libre albedrío, hack, pinganillo, auricular, micrófono, DIY, lonchafinismo, buscarse la vida
---
#### Consideraciones previas
Aquí en el subsuelo el común denominador es considerar que poco pinta un grupo de gente a campo través todos armados con radios y de cada una de las radios un cablecito a cada uno de esos dos pámpanos que los humanos también llamamos orejas. Enredándose, entreteniéndose, parándose a cada avería, enfadándose, aislándose.
Y peor aún, previo desembolso y derroche, emplear no ya el auricular/micrófono (pinganillo) que viene de serie, sino una especie de orejeras de diskjokey que, debido a su obsceno precio, suelen ser falsificaciones de los chinos. Todo muy táctico, muy cool.
> Como inciso, creo recordar que la ley dice que no se pueden empuñar armas con las orejas tapadas con cascos de música... algo así...; diga ésta (la ley) lo que diga, esto son bolitas. Mas como hay tanto **sumiso** a la ley, cabe recordarlo.  

En el monte hay que ir ligero, con lo puesto, lo indispensable. A la excelencia en el equipamiento se llega cuando ya no se puede suprimir nada más.  
En el monte, en cada unidad de combate hay un **radioperador**. Éste es el que ha de ocuparse de la radio, preferiblemente a pelo, sin artificios, a bajo volumen cuando es menester, imponiendo silencio cuando la unidad está trabajando. La unidad se comunica bien con señas, a pedradas si es menester. Cualquier cosa antes que funcionar como una chupipandi de niñatos con juguetes a los que han sacado de la ciudad a pasear por el monte y echarse unas fotos.

En fin, al turrón: Si vamos a emplear pinganillos y somos sensatos y lonchafinistas emplearemos los que de serie trae Puxing (o Pofung/Baofeng), que para algo se han pagado. Esa es otra, no vamos a tirar algo que funciona, ¿verdad que no?

#### Almohadillas.
El plástico del auricular "a pelo", al cabo de 24 horas es bastante molesto. Puede causar dolor en el conducto auditivo, amén de que -con el movimiento enérgico del cuerpo- se cae constantemente.  

Para solucionar esto en pro de una mayor "comodidad" y funcionalidad se puede utilizar teflón, sí esa "cosa" que usan los fontaneros.
O también las almohadillas de silicona de unos auriculares de música que se nos hayan roto (o las de recambio).  

- Extráiganse.  
- Córtese el "tubo" que encaja en el auricular.

<a href="/assets/img/2020-03-31-modificar_y_asegurar_pinganillo_puxing_baofeng_deserie/pinganillo_01.jpg" target="_blank"><img src="/assets/img/2020-03-31-modificar_y_asegurar_pinganillo_puxing_baofeng_deserie/pinganillo_01_g.jpg" alt="cortar excedente" width="250"/></a>

- Encájese el resto de la almohadilla en el auricular Puxing.

<a href="/assets/img/2020-03-31-modificar_y_asegurar_pinganillo_puxing_baofeng_deserie/pinganillo_02.jpg" target="_blank"><img src="/assets/img/2020-03-31-modificar_y_asegurar_pinganillo_puxing_baofeng_deserie/pinganillo_02_g.jpg" alt="cortar excedente" width="250"/></a>

Si antes de colocarlo en el pabellón auditivo lo humedecemos (échenle imaginación... saliva (no gargajo, que nos conocemos)) entrará mejor.

Si el diámetro del auricular resultara demasiado grande para nuestro conducto auditivo, basta con previamente rebajarlo con lija de grano gordo, que cunde más. Luego se le coloca la almohadilla de silicona o de teflón.


#### Asegurar el cable contra tirones.
Los tirones "estresan" al dispositivo. Si son grandes, o constantes, al final el pinganillo fallará. Dejará de escucharse, o de transmitir, o las dos cosas y, lo que es peor, existe la probabilidad nada remota de que adquiera "vida propia" entonces e interfiera constantemente en el canal en el que están comunicando. Problemas, problemas, llamadas de atención, incompetencia, malos rollos... Tanto fue el cántaro a la fuente que al final se acabó rompiendo.  

Un pequeño ovillo de "cordel de torcer" no falta en el bolsillo de cualquiera que bregue con relativa frecuencia por el monte. El hilo de torcer, que es muy resistente, sirve para muchas cosas, como todos vds saben, no hace falta que lo explique aquí.  

<a href="/assets/img/2020-03-31-modificar_y_asegurar_pinganillo_puxing_baofeng_deserie/pinganillo_03.jpg" target="_blank"><img src="/assets/img/2020-03-31-modificar_y_asegurar_pinganillo_puxing_baofeng_deserie/pinganillo_03_g.jpg" alt="cortar excedente" width="250"/></a>

Cójase un cabo de hilo de torcer.  
Téjase una **malla** asegurando los tramos que van desde el cuerpo del auricular al pulsador y del pulsador al conector. Malla de hilo, Auricular, pulsador y conector serán así los que soporten todas las tensiones.  

Si tejer una malla no se supiere, pese a que es como hacer una pulsera, algo que cualquier niña de parvulario sabría llevar a cabo, bastaría con cortar 2 cabos de cordel de torcer:  
El montaje en los "anclajes" (cuerpo del auricular, pulsador, conector) puede ser con pegamento o con un nudo fuerte.  
> 1 cabo desde el cuerpo del auricular al pulsador, dejando algo de holgura en éste.  
> 1 cabo desde el pulsador al cable que va hacia el conector (dejando algo de holgura en el cable).  

Dado que el cordel siempre (e intencionadamente) será más corto que el cable y que, por consiguiente, habrá un pequeño excedente de éste, si queremos que cordel y cable queden bien juntos en toda su longitud bastará con asegurar cada pocos centímetros con nudos o pegamento.